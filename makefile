.PHONY: clean
CC = g++
CFLAGS = -std=c++11 -Wfatal-errors -Wall
LIBS = -lgtest -lpthread
BIN = bin
SRC = src
TEST = test
OBJ = obj

DEPENDENCE = $(TEST)/ut_all.cpp $(TEST)/ut_timer.h $(SRC)/timer.cpp $(SRC)/timer.h \
	$(TEST)/ut_scheduler.h $(SRC)/scheduler.h $(SRC)/scheduler.cpp \
	$(TEST)/ut_schedule.h $(SRC)/schedule.h $(SRC)/schedule.cpp \
	$(TEST)/ut_job.h $(SRC)/job.h $(SRC)/job.cpp \
	$(TEST)/ut_task.h $(SRC)/task.h $(SRC)/task.cpp \
	$(TEST)/ut_processor.h $(SRC)/processor.h $(SRC)/processor.cpp \
	$(TEST)/ut_model.h $(SRC)/model.h $(SRC)/model.cpp \
	$(TEST)/ut_integration.h

OBJ_CODE = $(OBJ)/timer.o $(OBJ)/scheduler.o $(OBJ)/schedule.o $(OBJ)/job.o $(OBJ)/task.o \
	$(OBJ)/processor.o $(OBJ)/model.o

all: directories $(BIN)/ut_all $(BIN)/main

$(BIN)/main: $(SRC)/main.cpp $(OBJ_CODE)
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

$(BIN)/ut_all: $(TEST)/ut_all.cpp  $(OBJ_CODE)
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

$(OBJ)/%.o : $(SRC)/%.cpp $(DEPENDENCE) 
	$(CC) $(CFLAGS) -c $< -o $@

directories:
	mkdir -p $(BIN) $(OBJ)

clean:
	rm -rf $(BIN) $(OBJ)
