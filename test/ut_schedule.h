#ifndef UT_SCHEDULE_H
#define UT_SCHEDULE_H

#include "../src/schedule.h"

TEST(Schedule, Initialize) {
	time_t endTime = time(&endTime) + 1;
	int jid = 0;
	Schedule schedule(endTime, jid);
	ASSERT_EQ(endTime, schedule.getTime());
	ASSERT_EQ(jid, schedule.getJid());
}

#endif