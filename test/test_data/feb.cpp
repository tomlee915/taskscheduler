#include <iostream>

unsigned int feb (unsigned int n);

int main(int argc, char * argv[]) {
	int n = 45;
	std::cout << "Fibonacci sequence of " << n << " : " << feb(n) << std::endl;
}

unsigned int feb (unsigned int n) {
	if (n == 1 || n == 0)
		return 1;
	return feb(n - 1) + feb(n - 2);
}
// 1 1 2 3 5 8 13 21 34 55 89