#include <iostream>
#include <chrono>
#include <thread>

int main (int argc, char** argv)
{
	std::this_thread::sleep_for(std::chrono::seconds(7));
	std::cout << "Sleep for 7 sec" << std::endl;
	return 0;
}
