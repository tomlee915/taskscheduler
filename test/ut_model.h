#ifndef UT_MODEL_H
#define UT_MODEL_H

#include <sstream>
#include "../src/model.h"

TEST(Model, AddJob) {
	Model model;
	std::string path = "./test/test_data/hello";
	std::string name = "hello";
	model.addJob(path, name);
	ASSERT_ANY_THROW(model.addJob(path, name));
}

TEST(Model, GetJobList) {
	Model model;
	std::string path = "./test/test_data/hello";
	std::string name = "hello1";
	model.addJob(path, name);
	name = "hello2";
	model.addJob(path, name);
	name = "hello3";
	model.addJob(path, name);

	std::vector<Job *> jobs = model.getJobList();
	ASSERT_EQ(3, jobs.size());
	ASSERT_EQ("hello1", jobs[0]->getName());
	ASSERT_EQ("hello2", jobs[1]->getName());
	ASSERT_EQ("hello3", jobs[2]->getName());
}

TEST(Model, AddSchedule) {
	Model model;
	std::string path = "./test/test_data/hello";
	std::string name = "hello1";
	model.addJob(path, name);
	name = "hello2";
	model.addJob(path, name);
	name = "hello3";
	model.addJob(path, name);

	time_t endTime = time(&endTime);
	model.addSchedule(endTime, "hello1");
	model.addSchedule(endTime, "hello3");
	//model.addSchedule(endTime, "hello2");
	ASSERT_ANY_THROW(model.addSchedule(endTime, "no such job"));

	std::vector<Schedule *> schedules = model.getScheduleList();

	ASSERT_EQ(2, schedules.size());
	ASSERT_EQ(endTime, schedules[0]->getTime());
	ASSERT_EQ(endTime, schedules[1]->getTime());
	//ASSERT_EQ(endTime, schedules[2]->getTime());
}

#include <iostream>
TEST(Model, Update) {
	Model model;
	std::string path = "./test/test_data/hello";
	std::string name = "hello1";
	model.addJob(path, name);
	time_t endTime = time(&endTime) + 1;

	std::ostringstream outputStream(std::ostringstream::ate);
	model.setOutputStream(&outputStream);

	model.addSchedule(endTime, "hello1");
	std::this_thread::sleep_for (std::chrono::seconds(1));
	ASSERT_EQ("Hello, World!\n", outputStream.str());
}


#endif
