#ifndef UT_SCHEDULER_H
#define UT_SCHEDULER_H

#include "../src/scheduler.h"

TEST (Scheduler, Initialize) {
	Scheduler scheduler;
	time_t endTime = time(&endTime) + 1;
	scheduler.add(endTime, 0);
	scheduler.add(endTime, 1);
	scheduler.add(endTime, 2);
	scheduler.add(endTime, 1);
	std::vector<Schedule *> schedules = scheduler.getScheduleList();
	ASSERT_EQ(4, schedules.size());
	ASSERT_EQ(0, schedules[0]->getJid());
	ASSERT_EQ(1, schedules[1]->getJid());
	ASSERT_EQ(2, schedules[2]->getJid());
	ASSERT_EQ(1, schedules[3]->getJid());
}

TEST (Scheduler, ReturnList) {
	Scheduler scheduler;
	time_t endTime = time(&endTime) + 1;
	scheduler.add(endTime, 1);
	scheduler.add(endTime, 1998);
	std::vector<int> jids = scheduler.getJidList(endTime);
	ASSERT_EQ(2, jids.size());
	ASSERT_EQ(1, jids[0]);
	ASSERT_EQ(1998, jids[1]);
}
#endif