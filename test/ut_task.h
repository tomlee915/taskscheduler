#ifndef UT_TASK_H
#define UT_TASK_H

#include "../src/task.h"
#include "../src/job.h"

TEST(Task, Initialize) {
	Task task;
	std::string path = "./test/test_data/hello";
	std::string name = "hello";
	int id = task.createJob(path, name);
	ASSERT_EQ(0, id);

	path = "./test/test_data/hello";
	id = task.createJob(path, name);
	ASSERT_EQ(1, id);
}

TEST(Task, GetJob) {
	Task task;
	std::string path = "./test/test_data/hello";
	std::string name = "hello";
	int id = task.createJob(path, name);
	Job *job = task.getJob(id);
	ASSERT_EQ(path, job->getProgramPath());
}

TEST(Task, Search) {
	Task task;
	std::string path = "./test/test_data/hello";
	std::string name = "hello";
	int id = task.createJob(path, name);
	ASSERT_EQ(id, task.search(name));
	ASSERT_EQ(-1, task.search("No such job"));
}

TEST(Task, GetJobList) {
	Task task;
	std::string path = "./test/test_data/hello";
	std::string name = "hello1";
	task.createJob(path, name);
	name = "hello2";
	task.createJob(path, name);
	name = "hello3";
	task.createJob(path, name);

	std::vector<Job *> jobs = task.getJobList();
	ASSERT_EQ("hello1", jobs[0]->getName());
	ASSERT_EQ("hello2", jobs[1]->getName());
	ASSERT_EQ("hello3", jobs[2]->getName());
}

#endif