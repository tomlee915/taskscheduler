#ifndef UT_INTEGRATION_H
#define UT_INTEGRATION_H

#include "../src/scheduler.h"
#include "../src/processor.h"
#include "../src/job.h"
#include "../src/timer.h"


TEST(Integration, TimerNotify) {
	Scheduler scheduler;
	Task task;
	Processor processor(&scheduler, &task);
	scheduler.setProcessor(&processor);
	std::string path = "./test/test_data/hello";
	std::string name = "hello";
	int id = task.createJob(path, name);
	time_t endTime = time(&endTime) + 1;
	scheduler.add(endTime, id);
	std::this_thread::sleep_for (std::chrono::seconds(1));
	ASSERT_EQ("Hello, World!\n", processor.getResult(endTime, id));
}

#endif