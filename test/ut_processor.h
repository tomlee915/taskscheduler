#ifndef UT_PROCESSOR_H
#define UT_PROCESSOR_H

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include "../src/job.h"
#include "../src/scheduler.h"
#include "../src/processor.h"

TEST(Processor, Initialize) {
	Scheduler scheduler;
	Task task;
	Processor processor(&scheduler, &task);
	std::string path = "./test/test_data/hello";
	std::string name = "hello";
	task.createJob(path, name);

	time_t time = std::time(&time);
	scheduler.add(time, 0);
	ASSERT_ANY_THROW(processor.getResult(time, 0));

	processor.createProcess(time, 0);
	std::this_thread::sleep_for (std::chrono::seconds(1));
	ASSERT_EQ("Hello, World!\n", processor.getResult(time, 0));
}

TEST(Processor, GetModel){
	Model model;

	Scheduler scheduler;
	Task task;
	Processor processor(&scheduler, &task);
	processor.setModel(&model);
	ASSERT_EQ(&model, processor.getModel());
}

#endif
