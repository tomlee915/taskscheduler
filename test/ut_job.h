#ifndef UT_JOB_H
#define UT_JOB_H

#include "../src/job.h"

TEST(Job, Initialize) {
	std::string path = "./test/test_data/hello";
	int jid = 0;
	std::string name = "hello";
	Job job(path, jid, name);
	ASSERT_EQ(path, job.getProgramPath());
	ASSERT_EQ(jid, job.getId());
}

TEST(Job, GetName) {
	std::string path = "./test/test_data/hello";
	int jid = 0;
	std::string name = "hello";
	Job job(path, jid, name);
	ASSERT_EQ(name, job.getName());
}

#endif