#include <gtest/gtest.h>
#include "ut_timer.h"
#include "ut_scheduler.h"
#include "ut_schedule.h"
#include "ut_job.h"
#include "ut_task.h"
#include "ut_processor.h"
#include "ut_model.h"
#include "ut_integration.h"

int main(int argc, char ** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
