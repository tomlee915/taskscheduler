#ifndef UT_TIMER_H
#define UT_TIMER_H

#include <ctime>

#include "../src/timer.h"
#include "../src/scheduler.h"

TEST (Timer, Initialize) {
	time_t currentTime = time(&currentTime);
	Timer timer(currentTime, 0);
	ASSERT_EQ(currentTime, timer.getEndTime());
}

TEST (Timer, Start) {
	time_t endTime = time(&endTime) + 1;
	Timer timer(endTime, 0);
	timer.start();
	ASSERT_EQ(endTime, std::time(nullptr));
}

#endif
