#include "timer.h"
#include <thread>
#include <ctime>
#include <sstream>

Timer::Timer(long int inputTime, int jid) {
	endTime = inputTime;
	processor = nullptr;
	this->jid = jid;
}

void Timer::attatch(Processor * processor) {
	this->processor = processor;
}

void Timer::start() const {
	std::this_thread::sleep_until(std::chrono::system_clock::from_time_t(endTime));
	notify();
}

void Timer::notify() const {
	if (processor != nullptr)
		processor->createProcess(endTime, jid);
}

long int Timer::getEndTime() const{
	return endTime;
}
