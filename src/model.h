#ifndef MODEL_H
#define MODEL_H

#include <ctime>
#include <vector>
#include <ostream>

#include "job.h"
#include "processor.h"
#include "schedule.h"
#include "scheduler.h"
#include "task.h"
#include "timer.h"

class Processor;
class Schedule;
class Scheduler;
class Task;
class Job;
class Timer;

class Model {
public:
	Model();
	~Model();
	void addJob(std::string, std::string);
	void addSchedule(time_t, std::string);
	void addSchedule(time_t, std::string, int);
	std::vector<Job *> getJobList() const;
	std::vector<Schedule *> getScheduleList() const;
	void setOutputStream(std::ostream *);
	void writeOutputStream(time_t, int);
private:
	Scheduler *scheduler;
	Task *task;
	Processor *processor;
	std::ostream * outputStream;
};


#endif
