#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <map>
#include <mutex>

#include "job.h"
#include "model.h"
#include "schedule.h"
#include "scheduler.h"
#include "task.h"
#include "timer.h"

class Model;
class Schedule;
class Scheduler;
class Task;
class Job;
class Timer;

class Processor {
public:
	Processor(Scheduler *, Task *);
	~Processor();
	void createProcess(time_t, int);
	std::string getResult(time_t, int);
	void setModel(Model *);
	Model * getModel() const;
private:
	void InsertResult(Schedule *schedule);
	void execute(Schedule* schedule, std::string programPath);
	Scheduler * scheduler;
	std::map<Schedule*, std::string> result;
	Task * task;
	Model * model;
	std::mutex threadLock;
};

#endif
