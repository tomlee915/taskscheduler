#include "task.h"
#include "job.h"

Task::Task() { }

Task::~Task() {
	for (unsigned i = 0; i < jobs.size(); ++i) {
		delete jobs[i];
	}
}

int Task::createJob(std::string  path, std::string name) {
	int id = jobs.size();
	Job* job = new Job(path, id, name);
	jobs.push_back(job);
	return id;
}

size_t Task::getSize() const {
	return jobs.size();
}

Job * Task::getJob(int id) const {
	return jobs[id];
}

int Task::search(std::string name) const {
	for(unsigned i = 0; i < jobs.size(); ++i)
		if (jobs[i]->getName() == name)
			return jobs[i]->getId();
	return -1;
}

std::vector<Job *> Task::getJobList() const {
	return jobs;
}