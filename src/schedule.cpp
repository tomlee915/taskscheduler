#include "schedule.h"

Schedule::Schedule(time_t time, int jid) : time(time), jid(jid) {
	loopSec = 0;
}

Schedule::Schedule(time_t time, int jid, int loopSec) : time(time), jid(jid), loopSec(loopSec) { }

time_t Schedule::getTime() const {
	return time;
}

int Schedule::getJid() const {
	return jid;
}

int Schedule::getLoopSec() const {
	return loopSec;
}
