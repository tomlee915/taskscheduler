#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <string>

#include "job.h"
#include "model.h"
#include "processor.h"
#include "scheduler.h"
#include "task.h"
#include "timer.h"

class Model;
class Processor;
class Scheduler;
class Task;
class Job;
class Timer;

class Schedule {
public:
	Schedule(time_t, int);
	Schedule(time_t, int, int);
	time_t getTime() const;
	int getJid() const;
	int getLoopSec() const;
private:
	time_t time;
	int jid;
	int loopSec;
};

#endif
