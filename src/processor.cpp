#include <cstdio>

#include "processor.h"

Processor::Processor(Scheduler *scheduler, Task *task) : scheduler(scheduler), task(task) {
	model = nullptr;
}

Processor::~Processor() {
	for(std::map<Schedule*, std::string>::iterator itr = result.begin(); itr != result.end(); ++itr) {
		delete itr->first;
	}
}

void Processor::execute(Schedule* schedule, std::string programPath) {
	FILE *pipe = popen(programPath.c_str(), "r");
	if (!pipe) {
		return;
	}

	char buffer[256];
	while(fgets(buffer, sizeof(buffer), pipe) != NULL) {
		result[schedule] += std::string(buffer);
	}

	time_t endTime = time(&endTime);
	std::string endInfo = "Program "+ task->getJob(schedule->getJid())->getName() +" end at: " + std::string(std::ctime(&endTime));
	result[schedule] += endInfo;

	pclose(pipe);
	threadLock.lock();
	if (model != nullptr) {
		model->writeOutputStream(schedule->getTime(), schedule->getJid());
	}
	threadLock.unlock();
}

void Processor::createProcess(time_t time, int jid) {
	threadLock.lock();
	Schedule * schedule = scheduler->getSchedule(time, jid);
	int loopSec = schedule->getLoopSec();
	if (loopSec != 0)
		scheduler->add(time + loopSec, jid, loopSec);
	InsertResult(schedule);
	std::string programPath = task->getJob(jid)->getProgramPath();
	threadLock.unlock();
	execute(schedule, programPath);
}

void Processor::InsertResult(Schedule *schedule)
{
	time_t startTime = time(&startTime);
	std::string startInfo = "Program "+ task->getJob(schedule->getJid())->getName() +" start at: " + std::string(std::ctime(&startTime));
	result.insert(std::pair<Schedule *, std::string> (schedule, startInfo));
}

std::string Processor::getResult(time_t time, int jid) {
	for(std::map<Schedule*, std::string>::iterator itr = result.begin(); itr != result.end(); ++itr) {
		if (itr->first->getTime() == time && itr->first->getJid() == jid)
			return itr->second;
	}
	throw std::string("Cannot find the result");
}

void Processor::setModel(Model * mod){
	model = mod;
}

Model * Processor::getModel() const{
	return model;
}
