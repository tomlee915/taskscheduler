#include "job.h"

Job::Job (std::string path, int id, std::string name) : path(path), id(id), name(name) { }

std::string Job::getProgramPath() const {
	return path;
}

int Job::getId() const {
	return id;
}

std::string Job::getName() const {
	return name;
}