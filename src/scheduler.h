#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <vector>

#include "job.h"
#include "model.h"
#include "processor.h"
#include "schedule.h"
#include "task.h"
#include "timer.h"

class Model;
class Processor;
class Schedule;
class Task;
class Job;
class Timer;

class Processor;
class Timer;

class Scheduler {
public:
	Scheduler();
	void setProcessor(Processor * proc);
	void add(time_t, int);
	void add(time_t, int, int);
	size_t getSize() const;
	std::vector<int> getJidList(time_t);
	std::vector<Schedule *> getScheduleList() const;
	Schedule * getSchedule(time_t, int);
private:
	void startTimer(time_t, int);
	std::vector<Schedule *> schedules;
	Processor * processor;
};

#endif
