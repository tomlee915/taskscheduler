#include "scheduler.h"
#include <thread>

Scheduler::Scheduler() {
	processor = nullptr;
}

void Scheduler::setProcessor(Processor * proc) {
	processor = proc;
}

void Scheduler::add(time_t time, int jid) {
	Schedule* schedule = new Schedule(time, jid);
	schedules.push_back(schedule);
	startTimer(time, jid);
}

void Scheduler::add(time_t time, int jid, int loopSec) {
	Schedule* schedule = new Schedule(time, jid, loopSec);
	schedules.push_back(schedule);
	startTimer(time, jid);
}

void Scheduler::startTimer(time_t time, int jid) {
	Timer timer(time, jid);
	timer.attatch(processor);
	std::thread timerRun(&Timer::start, timer);
	timerRun.detach();
}

size_t Scheduler::getSize() const {
	return schedules.size();
}

std::vector<int> Scheduler::getJidList(time_t time) {
	std::vector<int> result;
	for (std::vector<Schedule *>::iterator itr = schedules.begin(); itr != schedules.end(); ++itr) {
		if((**itr).getTime() == time){
			result.push_back((**itr).getJid());
		}
	}
	return result;
}

std::vector<Schedule *> Scheduler::getScheduleList() const {
	return schedules;
}

Schedule * Scheduler::getSchedule(time_t time, int jid) {
	for (std::vector<Schedule*>::iterator itr = schedules.begin(); itr != schedules.end(); ++itr) {
		if ((*itr)->getTime() == time && (*itr)->getJid() == jid)
			return *itr;
	}
	return nullptr;
}
