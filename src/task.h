#ifndef TASK_H
#define TASK_H

#include <vector>

#include "job.h"
#include "model.h"
#include "processor.h"
#include "schedule.h"
#include "scheduler.h"
#include "timer.h"

class Model;
class Processor;
class Schedule;
class Scheduler;
class Job;
class Timer;

class Task {
public:
	Task();
	~Task();
	int createJob(std::string, std::string);
	size_t getSize() const;
	Job * getJob(int) const;
	int search(std::string) const;
	std::vector<Job *> getJobList() const;
private:
	std::vector<Job *> jobs;
};

#endif