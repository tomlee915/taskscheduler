#include <iostream>
#include <sstream>
#include <thread>

#include "model.h"

void addJob(Model *);
void addSchedule(Model *);
time_t convertTime(int, int, int, int, int, int);

int main(int argc, char** argv) {
	Model model;
	model.setOutputStream(&std::cout);
	std::string instr;
	std::cout << "Enter 'add_job' or 'add_schedule' or 'quit'" << std::endl;
	while(std::cin >> instr) {
		if (instr == "quit")
			break;
		else if (instr == "add_job")
			addJob(&model);
		else if (instr == "add_schedule")
			addSchedule(&model);
		else
			std::cout << "Unknown instruction :(" << std::endl;
		std::cout << "Enter 'add_job' or 'add_schedule' or 'quit'" << std::endl;
	}
	return 0;
}

void addJob(Model *model) {
	std::string path, name;

	std::cout << "Enter the name of program: ";
	std::cin >> name;
	std::cout << "Enter the path of the program: ";
	std::cin >> path;
	try {
		model->addJob(path, name);
		std::cout << "add job succeed" << std::endl;
	} catch (std::string msg) {
		std::cout << msg << std::endl;
	}
}

void addSchedule(Model *model) {
	int year, month, day, hour, min, sec, loopSec;
	std::string name, isloop;
	std::cout << "Enter the program name will be run: ";
	std::cin >> name;
	std::cout << "Should program loop? (y/n)";
	std::cin >> isloop;
	if (isloop == "y") {
		std::cout << "Loop every ___ sec:";
		std::cin >> loopSec;
	}
	std::cout << "Enter the time program will be run (YYYY MM DD hh mm ss): ";
	std::cin >> year >> month >> day >> hour >> min >> sec;
	time_t time = convertTime(year, month, day, hour, min, sec);
	try {
		if (isloop == "y")
			model->addSchedule(time, name, loopSec);
		else
			model->addSchedule(time, name);
		std::cout << "add schedule succeed" << std::endl;
	} catch (std::string msg) {
		std::cout << msg << std::endl;
	}
}

time_t convertTime(int year, int month, int day, int hour, int min, int sec) {
	time_t rawtime = time(&rawtime);
	struct tm * timeinfo = localtime (&rawtime);

	// struct tm::tm_year: years since 1900
	timeinfo->tm_year = year - 1900;
	timeinfo->tm_mon = month - 1;
	timeinfo->tm_mday = day;
	timeinfo->tm_hour = hour - 8;
	timeinfo->tm_min = min;
	timeinfo->tm_sec = sec;

	return timegm(timeinfo);
}
