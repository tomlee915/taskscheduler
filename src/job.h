#ifndef JOB_H 
#define JOB_H

#include <string>
#include "model.h"
#include "processor.h"
#include "schedule.h"
#include "scheduler.h"
#include "task.h"
#include "timer.h"

class Model;
class Processor;
class Schedule;
class Scheduler;
class Task;
class Timer;

class Job {
public:
	Job(std::string, int, std::string);
	std::string getProgramPath() const;
	int getId() const;
	std::string getName() const;
private:
	std::string path;
	int id;
	std::string name;
};

#endif