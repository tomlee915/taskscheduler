#ifndef TIMER_H
#define TIMER_H

#include <string>

#include "job.h"
#include "model.h"
#include "processor.h"
#include "schedule.h"
#include "scheduler.h"
#include "task.h"

class Model;
class Processor;
class Schedule;
class Scheduler;
class Task;
class Job;

class Timer {
public:
	Timer(time_t, int);
	void attatch(Processor * processor);
	time_t getEndTime() const;
	void start() const;
	void notify() const;
private:
	time_t endTime;
	Processor * processor;
	int jid;
};

#endif
