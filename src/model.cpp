#include "model.h"

Model::Model() {
	scheduler = new Scheduler();
	task = new Task();
	processor = new Processor(scheduler, task);
	scheduler->setProcessor(processor);
	processor->setModel(this);
}

Model::~Model() {
	delete scheduler;
	delete task;
	delete processor;
}

void Model::addJob(std::string path, std::string name) {
	int jid = task->search(name);
	if (jid >= 0)
		throw std::string("job already exists");
	task->createJob(path, name);
}

void Model::addSchedule(time_t time, std::string name) {
	int jid = task->search(name);
	if(jid >= 0)
		scheduler->add(time, jid);
	else
		throw std::string("no such job");
}

void Model::addSchedule(time_t time, std::string name, int loopSec) {
	int jid = task->search(name);
	if(jid >= 0)
		scheduler->add(time, jid, loopSec);
	else
		throw std::string("no such job");
}

std::vector<Job *> Model::getJobList() const {
	return task->getJobList();
}

std::vector<Schedule *> Model::getScheduleList() const {
	return scheduler->getScheduleList();
}

void Model::setOutputStream(std::ostream * outputStream) {
	this->outputStream = outputStream;
}

void Model::writeOutputStream(time_t time, int jid) {
	(*outputStream) << processor->getResult(time, jid);
}
